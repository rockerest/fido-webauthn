var express = require( "express" );
var compression = require( "compression" );
var morgan = require( "morgan" );
var path = require( "path" );

var app = express();

const PORT = 9876;

app.use( compression() );
app.use( morgan( "tiny" ) );

app.use( express.static( "public", {
    "fallthrough": true
} ) );
app.use( ( req, res ) => {
    if( !res.headersSent ){
        res.status( 200 ).sendFile( path.join( process.cwd(), "/public/index.html" ) ); // eslint-disable-line no-undef
    }
} );

app.listen( PORT );

console.log( `👂🏼 The web server is listening at http://localhost:${PORT}` );
